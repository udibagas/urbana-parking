// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    compatibilityDate: "2024-11-01",
    experimental: { appManifest: false },
    devtools: { enabled: true },
    ssr: false,
    modules: ["@element-plus/nuxt", "nuxt-auth-sanctum", "@pinia/nuxt"],
    plugins: ["@/plugins/mask.js"],
    // pinia: {
    //     storesDirs: ["./stores/**"],
    // },
    css: ["@/assets/app.css"],
    runtimeConfig: {
        public: {
            apiBase: process.env.NUXT_PUBLIC_API_BASE || "/",
            sanctum: {
                baseUrl: process.env.NUXT_PUBLIC_API_BASE || "/",
                endpoints: {
                    login: "/api/login",
                    logout: "/api/logout",
                    user: "/api/me",
                },
                redirect: {
                    onLogout: "/login",
                },
                globalMiddleware: {
                    enabled: true,
                    allow404WithoutAuth: true,
                },
            },
        },
    },
});
