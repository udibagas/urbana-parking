export function getSiklus(unit) {
  return {
    days: "Hari",
    weeks: "Minggu",
    months: "Bulan",
    years: "Tahun",
  }[unit];
}
