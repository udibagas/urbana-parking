<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUhfTagNumberOnVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('member_vehicles', function (Blueprint $table) {
            $table->string('uhf_tag_number')->nullable();
            $table->date('uhf_expiry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_vehicles', function (Blueprint $table) {
            $table->dropColumn([
                'uhf_tag_number',
                'uhf_expiry_date'
            ]);
        });
    }
}
