@extends('layouts.print')

@section('content')
    <h3 class="text-center text-2xl mb-3">TRANSAKSI PARKIR </h3>

    <table class="table-auto min-w-full">
        <thead>
            <tr>
                <th class="border px-3">#</th>
                <th class="border px-3">Nomor Tiket</th>
                <th class="border px-3">Jenis Kendaraan/<br> Plat Nomor</th>
                <th class="border px-3">Nomor Kartu/ Member/ Group</th>
                <th class="border px-3">Waktu Masuk/ Keluar</th>
                <th class="border px-3">Gate Masuk/ Keluar</th>
                <th class="border px-3">Durasi</th>
                <th class="border px-3">Tarif/Denda</th>
                <th class="border px-3">Shift</th>
                <th class="border px-3">Operator</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $d)
                <tr>
                    <td class="border px-3">{{ $d['No.'] }}</td>
                    <td class="border px-3">{{ $d['Nomor Tiket'] }}</td>
                    <td class="border px-3">{{ $d['Jenis Kendaraan'] }} <br> {{ $d['Plat Nomor'] }}</td>
                    <td class="border px-3">{{ $d['Nomor Kartu'] }} <br>{{ $d['Member'] }} <br> {{ $d['Group'] }}</td>
                    <td class="border px-3">{{ $d['Waktu Masuk'] }} <br></td>
                    <td class="border px-3">{{ $d['Gate Masuk'] }} <br>{{ $d['Gate Keluar'] }}</td>
                    <td class="border px-3">{{ $d['Durasi'] }}</td>
                    <td class="border px-3">{{ $d['Tarif'] }} <br>{{ $d['Denda'] }}</td>
                    <td class="border px-3">{{ $d['Shift'] }}</td>
                    <td class="border px-3">{{ $d['Operator'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
