card_types = {
    "01": "STI Luminos Card",
    "02": "eMoney Mandiri Card",
    "03": "Brizzi BRI Card",
    "04": "Tapcash BNI Card",
    "05": "Flazz BCA Card",
    "06": "Jakcard DKI Card",
    "07": "Bank Nobu Card",
    "08": "Bank Mega Card",
    "FF": "Type A or Mifare Card",
}

validity_flags = {
    "00": "Card number is invalid",
    "01": "Card number is valid, but card balance is invalid",
    "02": "Card balance is valid, but card number is invalid",
    "03": "Both card number and card balance is valid",
}


def string_to_hex(string):
    return ''.join(format(ord(c), '02x') for c in string)


class Card:
    def __init__(self, type="", uid="", data_validity_flag="", number="", balance=0):
        self.type = type
        self.uid = uid
        self.data_validity_flag = data_validity_flag
        self.number = number
        self.balance = balance

    @staticmethod
    def create(data):
        data = string_to_hex(data)
        card_type = data[0:2]
        card_uid = data[2:22]
        card_data_validity_flag = data[22:24]
        card_number = data[24:40]
        card_balance = int(data[40:48], 16)

        return Card(
            card_type,
            card_uid,
            card_data_validity_flag,
            card_number,
            card_balance
        )

    def reset(self):
        self.type = ""
        self.uid = ""
        self.data_validity_flag = ""
        self.number = ""
        self.balance = 0

    @property
    def type_description(self):
        return card_types.get(self.type, "Unknown")

    @property
    def insufficient_balance(self):
        return self.data_validity_flag == "01"

    @property
    def invalid_card_number(self):
        return self.data_validity_flag in ["00", "02"]

    @property
    def is_valid(self):
        return self.data_validity_flag == "03"

    @property
    def data_validity_flag_description(self):
        return validity_flags.get(self.data_validity_flag, "Unknown")

    @property
    def balance_formatted(self):
        return f"Rp {self.balance:,.0f}"
