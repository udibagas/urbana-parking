<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $user = User::where('status', 1)
            ->where(function ($q) use ($request) {
                return $q->where('name', $request->email)
                    ->orWhere('email', $request->email);
            })->first();

        if ($user && password_verify($request->password, $user->password)) {
            // simpan log jika bukan controller
            if ($user->name !== 'controller') {
                UserLog::create([
                    'user_id' => $user->id,
                    'action' => 'LOGIN'
                ]);
            }

            Auth::login($user, true);

            return response()->json([
                'token' => $user->createToken($request->device_name ?: 'web', ['*'], now()->addDay())->plainTextToken,
                'user' => $user
            ]);
        }

        return response()->json([
            'message' => 'Username atau password salah',
        ], 403);
    }

    public function logout(Request $request)
    {
        UserLog::create([
            'user_id' => Auth::user()->id,
            'action' => 'LOGOUT'
        ]);

        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return 'OK';
    }

    public function me(Request $request)
    {
        return response()->json([...$request->user()->toArray(), 'user' => $request->user()]);
    }
}
