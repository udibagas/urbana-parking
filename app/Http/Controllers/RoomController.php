<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Models\User;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware("role:" . User::ROLE_ADMIN)->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Room::orderBy('name', 'asc')
            ->withCount('members')
            ->when($request->keyword, function ($q) use ($request) {
                $q->where('name', 'LIKE', "%{$request->keyword}%")
                    ->orWhere('description', 'LIKE', "%{$request->keyword}%");
            });

        return $request->paginated ? $data->paginate($request->pageSize) : $data->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoomRequest $request)
    {
        $names = array_unique(explode(',', $request->name));

        if (count($names) > 1) {
            $registeredRooms = Room::pluck('name')->toArray();

            $names = array_filter($names, function ($name) use ($registeredRooms) {
                return !in_array($name, $registeredRooms);
            });

            Room::insert(array_map(function ($name) use ($request) {
                return [
                    "name" => trim($name),
                    "description" => $request->description,
                    "created_at" => now(),
                    "updated_at" => now()
                ];
            }, $names));
        } else Room::create($request->all());

        return ['message' => 'Room telah ditambahkan'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        return $room;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoomRequest $request, Room $room)
    {
        $room->update($request->all());
        return ['message' => 'Room telah diupdate', 'room' => $room];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();
        return ['message' => "Room telah dihapus"];
    }
}
