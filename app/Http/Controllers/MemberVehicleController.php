<?php

namespace App\Http\Controllers;

use App\Models\MemberVehicle;
use Illuminate\Http\Request;

class MemberVehicleController extends Controller
{
    public function destroy(MemberVehicle $memberVehicle)
    {
        $memberVehicle->delete();
        return ['message' => 'Data berhasil disimpan'];
    }

    public function checkUhf(Request $request)
    {
        $request->validate(['uhf_tag_number' => 'required']);

        $vehicle = MemberVehicle::where('uhf_tag_number', $request->uhf_tag_number)->first();

        if (!$vehicle) {
            return response(['message' => 'Kendaraan tidak terdaftar'], 404);
        }

        if ($vehicle->expired) {
            return response(['message' => 'Tag UHF Expired'], 404);
        }

        return $vehicle;
    }
}
