<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use App\Http\Requests\MemberRequest;
use App\Models\GroupMember;
use App\Models\MemberVehicle;
use App\Models\ParkingTransaction;
use App\Models\Room;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:1')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Member::when($request->keyword, function ($q) use ($request) {
            $q->where(function ($q) use ($request) {
                $q->where('nama', 'LIKE', "%{$request->keyword}%")
                    ->orWhere('nomor_kartu', 'LIKE', "%{$request->keyword}%")
                    ->orWhereHas('room', function ($q) use ($request) {
                        $q->where('name', 'LIKE', "%{$request->keyword}%");
                    })
                    ->orWhereHas('vehicles', function ($q) use ($request) {
                        $q->where(function ($q) use ($request) {
                            $q->where('plat_nomor', 'LIKE', "%{$request->keyword}%")
                                ->orWhere('uhf_tag_number', 'LIKE', "%{$request->keyword}%");
                        });
                    });
            });
        })->when($request->columns, function ($q) use ($request) {
            $q->selectRaw($request->columns);
        })->when($request->status, function ($q) use ($request) {
            $q->whereIn('status', $request->status);
        })->when($request->card_type, function ($q) use ($request) {
            $q->whereIn('card_type', $request->card_type);
        })->when($request->group_member_id, function ($q) use ($request) {
            $q->whereIn('group_member_id', $request->group_member_id);
        })->when($request->expired == ['y'] || $request->expired == 'y', function ($q) {
            $q->whereRaw('expiry_date < DATE(NOW())');
        })->when($request->expired == ['n'] || $request->expired == 'n', function ($q) {
            $q->whereRaw('expiry_date >= DATE(NOW())');
        })->when($request->berbayar == ['y'] || $request->berbayar == 'y', function ($q) {
            $q->where('berbayar', 1);
        })->when($request->berbayar == ['n'] || $request->berbayar == 'n', function ($q) {
            $q->where('berbayar', 0);
        })->orderBy($request->sort_prop ?: 'nama', $request->sort_order ?: 'asc');

        $data = $request->paginated ? $data->paginate($request->pageSize) : $data->get();

        if ($request->action == 'export') {
            $siklus = [
                'days' => 'hari',
                'weeks' => 'minggu',
                'months' => 'bulan',
                'years' => 'tahun'
            ];

            return $data->map(function ($item, $index) use ($siklus) {
                return [
                    'No' => $index + 1,
                    'Nama' => $item->nama,
                    'Jenis' => $item->berbayar ? 'BERBAYAR' : 'GRATIS',
                    'Group' => $item->group->nama,
                    'Nomor Kartu' => $item->nomor_kartu,
                    'Plat Nomor' => implode(',', $item->vehicles->pluck('plat_nomor')->toArray()),
                    'Tanggal Daftar' => $item->register_date,
                    'Tanggal Kedaluarsa' => $item->expiry_date,
                    'Tarif' => $item->tarif,
                    'Siklus Bayar' => "{$item->siklus_pembayaran}  {$siklus[$item->siklus_pembayaran_unit]}",
                    'Nomor HP' => $item->phone,
                    'Transaksi Terakhir' => $item->last_transaction ?? '-',
                    'Status Kartu' => $item->expired ? 'KEDALUARSA' : 'BERLAKU',
                    'Status Anggota' => $item->status ? 'AKTIF' : 'NONAKTIF'
                ];
            });
        }

        return $request->action == 'print'
            ? view('member.print', ['data' => $data, 'setting' => Setting::first()])
            : $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberRequest $request)
    {
        $member = DB::transaction(function () use ($request) {
            $member = Member::create($request->all());
            $member->vehicles()->createMany($request->vehicles);
            return $member;
        });

        return ['message' => 'Data berhasil disimpan', 'data' => $member];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        return $member;
    }

    public function search(Request $request)
    {
        if (!$request->nomor_kartu && !$request->plat_nomor) {
            return response(['message' => 'Nomor Kartu/Plat Nomor darus diisi'], 500);
        }

        $member = Member::where('status', 1)
            ->when($request->nomor_kartu, function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('nomor_kartu', 'LIKE', '%' . $request->nomor_kartu)
                        ->orWhereHas('vehicles', function ($q) use ($request) {
                            $q->where('uhf_tag_number', $request->nomor_kartu)
                                ->where('uhf_expiry_date', '>=', date('Y-m-d'));
                        });
                });
            })->when($request->plat_nomor, function ($q) use ($request) {
                $q->whereHas('vehicles', function ($q) use ($request) {
                    $q->where('plat_nomor', 'LIKE', $request->plat_nomor);
                });
            })->first();

        if (!$member) {
            return response(['message' => 'Member tidak ditemukan'], 404);
        }

        // kalau cari berdasarkan kartu berarti di gate in, cari apa dia ada transaksi yg blm closed
        if ($request->nomor_kartu) {
            $unclosed = ParkingTransaction::where('nomor_kartu', 'LIKE', "%{$request->nomor_kartu}")
                ->whereNull('time_out')
                ->first();

            $setting = Setting::first();
            $member->unclosed = ($setting && $setting->must_checkout && $unclosed && $member->card_type != 'UHF') ? true : false;

            // kalau gak harus check out otomatis close trx sebelumnya
            if ($setting && !$setting->must_checkout && $unclosed) {
                $unclosed->update(['time_out' => now()]);
            }
        }

        return $member;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberRequest $request, Member $member)
    {
        DB::transaction(function () use ($request, $member) {
            $member->update($request->all());
            $member->vehicles()->delete();
            $member->vehicles()->createMany($request->vehicles);
        });

        return ['message' => 'Data berhasil disimpan', 'data' => $member];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        DB::transaction(function () use ($member) {
            $member->delete();
            $member->vehicles()->delete();
        });

        return ['message' => 'Member telah dihapus'];
    }

    public function import(Request $request)
    {
        DB::transaction(function () use ($request) {
            $siklus = [
                'hari' => 'days',
                'minggu' => 'weeks',
                'bulan' => 'months',
                'tahun' => 'years'
            ];

            foreach ($request->rows as $row) {
                $room = Room::firstOrCreate(['name' => $row['room']], ['name' => $row['room']]);
                $group = GroupMember::firstOrCreate(['nama' => $row['room']], ['nama' => $row['room']]);

                $member = Member::updateOrCreate(
                    ['nomor_kartu' => $row['nomor_kartu']],
                    [
                        'nama' => $row['nama'],
                        'nomor_kartu' => $row['nomor_kartu'],
                        'card_type' => $row['card_type'],
                        'status' => strtolower($row['status']) == 'aktif',
                        'expiry_date' => $row['expiry_date'] ?? date('Y-m-d'),
                        'phone' => $row['phone'],
                        'group_member_id' => $group->id,
                        'berbayar' => strtolower($row['berbayar']) == 'berbayar',
                        'siklus_pembayaran_unit' => $siklus[$row['siklus_pembayaran_unit']],
                        'register_date' => $row['register_date'] ?? date('Y-m-d'),
                        'siklus_pembayaran' => $row['siklus_pembayaran'],
                        'tarif' => $row['tarif'] ?? 0,
                        'room_id' => $room->id
                    ]
                );

                if ($row['jenis_kendaraan']) {
                    MemberVehicle::updateOrCreate(
                        ['plat_nomor' => $row['plat_nomor']],
                        [
                            'member_id' => $member->id,
                            'jenis_kendaraan' => $row['jenis_kendaraan'],
                            'plat_nomor' => $row['plat_nomor'],
                            'merk' => $row['merk'],
                            'tipe' => $row['tipe'],
                            'tahun' => $row['tahun'],
                            'warna' => $row['warna'],
                            'uhf_tag_number' => $row['uhf_tag_number'],
                            'uhf_expiry_date' => $row['uhf_expiry_date']
                        ]
                    );
                }
            }
        });

        return response()->json(['message' => 'Data member berhasil diimport']);
    }
}
