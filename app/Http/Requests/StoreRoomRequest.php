<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:rooms,name,' . $this->id,
            'description' => 'max:255'
        ];
    }

    public function attributes()
    {
        return [
            'name' => "Nama",
            'description' => 'Keterangan'
        ];
    }
}
