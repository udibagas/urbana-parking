<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'nama',
        'nomor_kartu',
        'card_type',
        'status',
        'expiry_date',
        'balance',
        'last_transaction',
        'email',
        'phone',
        'group_member_id',
        'berbayar',
        'siklus_pembayaran_unit',
        'register_date',
        'siklus_pembayaran',
        'tarif',
        'last_in',
        'last_out',
        'room_id'
    ];

    protected $appends = ['expired_in', 'expired', 'all_related_vehicles'];

    protected $with = ['vehicles', 'group', 'room'];

    public function getAllRelatedVehiclesAttribute()
    {
        return MemberVehicle::whereHas('member', function ($q) {
            $q->where('room_id', $this->room_id);
        })->select(['id', 'jenis_kendaraan', 'plat_nomor', 'uhf_expiry_date'])->get();
    }

    public function vehicles()
    {
        return $this->hasMany(MemberVehicle::class);
    }

    public function getExpiredInAttribute()
    {
        return (int) (new DateTime($this->expiry_date))->diff(new DateTime(date('Y-m-d')))->format('%a');
    }

    public function getExpiredAttribute()
    {
        return strtotime(date('Y-m-d')) > strtotime($this->expiry_date);
    }

    public function group()
    {
        return $this->belongsTo(GroupMember::class, 'group_member_id');
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function relatedMember()
    {
        return $this->room->members;
    }
}
