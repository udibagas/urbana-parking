<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $fillable = [
        'nama',
        'keterangan',
        'contact_person_name',
        'contact_person_email',
        'contact_person_phone'
    ];

    protected $hidden = [
        'contact_person_name',
        'contact_person_email',
        'contact_person_phone',
        'created_at',
        'updated_at'
    ];

    public function members()
    {
        return $this->hasMany(Member::class);
    }
}
