<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class MemberVehicle extends Model
{
    protected $fillable = [
        'member_id',
        'jenis_kendaraan',
        'plat_nomor',
        'merk',
        'tipe',
        'tahun',
        'warna',
        'uhf_tag_number',
        'uhf_expiry_date'
    ];

    protected $appends = ['expired', 'expired_in'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function getExpiredInAttribute()
    {
        return (int) (new DateTime($this->uhf_expiry_date))->diff(new DateTime(date('Y-m-d')))->format('%a');
    }

    public function getExpiredAttribute()
    {
        return strtotime(date('Y-m-d')) > strtotime($this->uhf_expiry_date);
    }
}
