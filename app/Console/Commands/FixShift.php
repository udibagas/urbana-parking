<?php

namespace App\Console\Commands;

use App\Models\ParkingTransaction;
use App\Models\Shift;
use Illuminate\Console\Command;

class FixShift extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shift:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix shift on parking transaction report based on shift setting';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $shifts = Shift::all();

        foreach ($shifts as $s) {
            $this->info("Updating shift {$s->nama}...");
            ParkingTransaction::whereRaw('TIME(time_in) BETWEEN ? AND ?', [$s->mulai . ':00', $s->selesai . ':59'])->update(['shift_id' => $s->id]);
            $this->info('DONE');
        }
    }
}
